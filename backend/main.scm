(use-modules (web server))
(use-modules (web request)
    (web response)
    (web uri))
(use-modules (ice-9 textual-ports))
(use-modules (srfi srfi-19))

(define messageFile "messages.txt")

(define (request-path-components request)
    (split-and-decode-uri-path (uri-path (request-uri request)))
)

; (define (read-file-content file-path)
; (define contents "none")

(define (read-file-content file-path)
(let ((port (open-input-file file-path)))
  (let ((contents (get-string-all port)))
    (close-port port)
    contents)))

(define (getISOdate)
    (define dateString
        (string-append
        (number->string (date-year (current-date)))
        "-"
        (number->string (date-month (current-date)))
        "-"
        (number->string (date-day (current-date)))
        "t"
        (number->string (date-hour (current-date)))
        ":"
        (number->string (date-minute (current-date)))
        ":"
        (number->string (date-second (current-date)))
        )
    )
    dateString
)

(define (writeMessage messageContent nick)
(define date (getISOdate))
(let ((port (open-file messageFile "a")))
  (put-string port (string-append "\n" date "; " nick ": " messageContent))
  (close-port port)))


(define (messageHandler request body)
    (display body)
    (display (request-path-components request))
    (display (equal? (request-path-components request) '("sendMessage")))

    (if (equal? body #f) ; TODO: Is there a more clever way to do this?
        (set! body "null")
        (set! body body)
    )

    (if (equal? (request-path-components request) '("sendMessage"))
        (begin
            (writeMessage body "name") ; TODO: Replace name with actual name
            (values '((content-type . (text/plain))) 
                "Message sent!"
            )
        ) (if (equal? (request-path-components request) '("messages"))
                (values '((content-type . (text/plain)))
                    (read-file-content messageFile)
                )
                (not-found request)
        )
    )
)

(run-server messageHandler)
