I want to build something practical using these technologies:

* Scheme (and posibly spiffy, too)
* React for JS
* Databases

Yeah, a webapp. I'll use scheme for the backend and it interfaces with the database. React, Sass, and tailwind could fit into the frontend, all assembled with typescript, and a single database at the centre, using postgreSQL. 

This whole techstack will be assembled with docker to make things simpler for me.

So, let's quickly run over things:

* Backend
    * Scheme
    * Spiffy for connections
    * SchemeQL and SchemeQL-postgresSQL for the databases
    * PostgreSQL to handle the database
* Frontend
    * Typescript
    * React
* Connective glue
    * Rest APIs

This project will have these features:

* Ability to view and download all chat messages as raw JSON
* A JSON-using API for chatting
* Ability to change username with previously-used ones needing authentication.
* Ability to send and edit messages within 5 minutes
* Clients being a "dumb terminal" doing the most basic things because we can't trust them.

If you remember, in 2023-12-24 i did design a similar app, but this one is going to be better.

And it'll be designed in a very modular way, too. 
