# docker build -t chatapp . && docker run -p 8080:8080 chatapp

FROM nginx:alpine

# Copy our code

COPY . .
COPY ./frontend/build /usr/share/nginx/html

# Install the required things first
RUN sudo apt install -y \
    nodejs \
    npm \
    chicken && \
    npm install react react-dom react-scripts typescript --save \
    sudo chicken-install spiffy

# Go prepare things to be ran

RUN cd frontend && \
    npm run build && \
    cd ../backend && \
    csc main.scm

# And finally run things

RUN 

# Expose port 80 for incoming web traffic
EXPOSE 80

# Command to start Nginx when the container starts
CMD ["nginx", "-g", "daemon off;", "csm", "./main.scm"]


