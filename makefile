# Program writer assumes absolutely no responsibility for any nonworking.

# Build

buildFront:
	cd frontend && npm run build

buildBack:
	cd backend && guile -s main.scm

build: buildFront buildBack

runBack:
	cd ./backend && guile -s main.scm

runFront:
	cd ./frontend && serve -s build -l 8081 &

run: runFront runBack

all: build run

