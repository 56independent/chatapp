import React, { useState } from 'react';

let username = "nick"

function TopBar(name) {
    return <h1>{name.name}</h1>
}

function TextMessage(prop) {
    return <p tooltip={prop.date}><b>{prop.user}</b> {prop.message}</p>
}

function TextBox() {
    const [message, setMessage] = useState('');

    const sendMessageFromBox = () => {
        console.log(message);
        sendMessage(message)
    };

    return (<>
        <input type="text" value={message} onChange={(e) => setMessage(e.target.value)}/>
        <button onClick={sendMessageFromBox} >Send!</button>
        </>)
}

export default function App() {
    const messages = requestMessages()
    console.log(messages)
    const messagesArray = messages.split("\n")
    let breakdownArray = []

    for (const message of messagesArray) {
        const breakdown = /^(.*?); (.*?): (.*?)$/gm.exec(message)
        breakdownArray.push(breakdown)
    }

    console.log(breakdownArray)

    let messagesJsx = breakdownArray.map(list => <TextMessage date={list[1]} user={list[2]} message={list[3]} key={list[1]}></TextMessage>)

    //messagesJsx = messagesJsx.slice(1)

    console.log(messagesJsx)

    return (<>
        <TopBar name={username}></TopBar>
        {messagesJsx}
        <TextBox></TextBox>
    </>)
}

/*
API crashcourse:

* To send a message simply send a post request with its content in the standard form, minus `date; `. This iwll be appended to the global message file
* To recieve messages, simply send a get request, which will return a newline-seperated list of messages in their standard form

Standard form is as follows:

date; username: message

TODO: Replace with a more robust API using JSON

*/

function requestMessages(){

return `2023-12-27t14:51:7; 56i: This is the messages channel
2023-12-27t14:53:1; 56i: We use it to send messages
2023-12-27t14:53:6; 56i: As you can see it works
2023-12-27t14:55:25; 56i: Even if not well`

    // TODO: This code does not seem to work...? Something with there being the index.html being returned instead of the code form the backend.

    let url = "127.0.0.1:8080/messages" // TODO: Replace with a more real URL
    let messages = ""

    fetch(url)
        .then(response => response.text())
        .then(data => {
            // Assuming the server returns a plaintext file named 'messages'
            console.log(data); // You can further process or display the content as needed
            messages = data
        })
        .catch(error => console.error('Error:', error));
    
    return messages
}

function sendMessage(message){
    fetch("127.0.0.1:8080/sendMessage", {
        method: "POST",
        body: "username: " + message,
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    }).then((response) => {console.log(response)})
}

